package esprit.com.SecondBack.metier;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import esprit.com.SecondBack.dao.CompteRepository;
import esprit.com.SecondBack.entities.Compte;


@Service
public class CompteMetierImp implements CompteMetier {

	@Autowired
	private CompteRepository compteRepository ;
	
	@Override
	public Compte AjouterCompte(Compte c) {
		
		c.setDateCration(new Date());
		return compteRepository.save(c);
	}

	

	@Override
	public Compte getCompte(String numCompte) {
		
		return compteRepository.getOne(numCompte);
	}

}
