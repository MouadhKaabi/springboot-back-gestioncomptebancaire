package esprit.com.SecondBack.metier;

import java.util.Date;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import esprit.com.SecondBack.dao.CompteRepository;
import esprit.com.SecondBack.dao.EmployeRepository;
import esprit.com.SecondBack.dao.OperationRepository;
import esprit.com.SecondBack.entities.Compte;
import esprit.com.SecondBack.entities.Employe;
import esprit.com.SecondBack.entities.Operation;
import esprit.com.SecondBack.entities.Retrait;
import esprit.com.SecondBack.entities.Versement;

@Service
public class OperationMetierImp implements OperationMetier {

	@Autowired
	private OperationRepository operationRepository ;
	
	@Autowired
	private CompteRepository compteRepository ;
	
	@Autowired
	private EmployeRepository employeRepository;
	
	
	@org.springframework.transaction.annotation.Transactional
	@Override
	public boolean verser(String numCompte, double montant, long codeEmploye) {
		Compte cp = compteRepository.getOne(numCompte);
		Employe emp = employeRepository.getOne(codeEmploye);
		
		Operation o = new Versement();
		
		o.setDatecreation(new Date());
		
		o.setMontant(montant);
		
		o.setCompte(cp);
		o.setEmploye(emp);
		
		operationRepository.save(o);	
		
		cp.setSolde((long) (cp.getSolde()+montant));
		
		
		return true;
	}

	@Override
	@Transactional
	public boolean retirer(String numCompte, double montant, long codeEmploye) {
		Compte cp = compteRepository.getOne(numCompte);
		Employe emp = employeRepository.getOne(codeEmploye);
		if (cp.getSolde()<montant) throw new RuntimeException("Solde insufissant");
		
		Operation o = new Retrait();
		
		o.setDatecreation(new Date());
		
		o.setMontant(montant);
		
		o.setCompte(cp);
		o.setEmploye(emp);
		
		operationRepository.save(o);	
		
		cp.setSolde((long) (cp.getSolde()-montant));
		return true;
	}

	@Override
	
	@Transactional
	public boolean virement(String cpt1, String cpt2, double montant, long codeEmploye) {
		retirer(cpt1, montant, codeEmploye);
		verser(cpt2, montant, codeEmploye);
		return true;
	}

}
