package esprit.com.SecondBack.metier;

public interface OperationMetier {
	
	public boolean verser(String numCompte ,double montant,long codeEmploye);
	
	public boolean retirer(String numCompte ,double montant,long codeEmploye);
	
	public boolean virement (String cpt1,String cpt2,double montant,long codeEmploye);

}
