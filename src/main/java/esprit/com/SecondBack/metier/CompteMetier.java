package esprit.com.SecondBack.metier;

import java.util.List;

import esprit.com.SecondBack.entities.Compte;

public interface CompteMetier {
	
	public Compte AjouterCompte (Compte c);
	
	public Compte getCompte (String numCompte);

}
