package esprit.com.SecondBack.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import esprit.com.SecondBack.dao.ClientRepository;
import esprit.com.SecondBack.entities.Client;

//les objets pour la couche metier 
@Service
public class ClientMetierImp implements ClientMetier {
	//injection des dependance 
	@Autowired
	private ClientRepository clientRepository ;
	@Override
	public Client AjouterClient(Client c) {
		
		return clientRepository.save(c);
	}

	@Override
	public List<Client> listClient() {
		// TODO Auto-generated method stub
		return clientRepository.findAll();
	}

}
