package esprit.com.SecondBack.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import esprit.com.SecondBack.dao.EmployeRepository;
import esprit.com.SecondBack.entities.Employe;
@Service
public class EmployeMerierImp implements EmployeMetier {

	@Autowired
	private EmployeRepository employeRepository ;
	@Override
	public Employe AjouterEmpl(Employe e) {
		// TODO Auto-generated method stub
		return employeRepository.save(e);
	}

	@Override
	public List<Employe> listemplEmployes() {
		// TODO Auto-generated method stub
		return employeRepository.findAll();
		
	}

}
