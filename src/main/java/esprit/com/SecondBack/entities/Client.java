package esprit.com.SecondBack.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Client implements Serializable {

	private static final long serialVersionUID = 1075116492787916953L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
	private long code ;
	
	private String nom ;
	
	@OneToMany(mappedBy="client",cascade = CascadeType.REMOVE,fetch = FetchType.EAGER)
	private Collection<Compte>Comptes ;

	public long getCode() {
		return code;
	}

	public void setCode(long code) {
		this.code = code;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	@JsonIgnore
	public Collection<Compte> getComptes() {
		return Comptes;
	}

	public void setComptes(Collection<Compte> comptes) {
		Comptes = comptes;
	}

	public Client() {
		super();
	}

	
	
	

}
