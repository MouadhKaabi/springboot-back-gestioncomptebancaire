package esprit.com.SecondBack.entities;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Groupe {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	 
	private long  numGroupe ; 
	
	private String nomGroupe ;
	
	@ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.REMOVE)
	private Collection<Employe>employes ;

	public long getNumGroupe() {
		return numGroupe;
	}

	public void setNumGroupe(long numGroupe) {
		this.numGroupe = numGroupe;
	}

	public String getNomGroupe() {
		return nomGroupe;
	}

	public void setNomGroupe(String nomGroupe) {
		this.nomGroupe = nomGroupe;
	}
	
	
	@JsonIgnore
	public Collection<Employe> getEmployes() {
		return employes;
	}

	public void setEmployes(Collection<Employe> employes) {
		this.employes = employes;
	}

	public Groupe() {
		super();
	}

	
	
	

}
