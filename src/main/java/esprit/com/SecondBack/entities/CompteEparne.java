package esprit.com.SecondBack.entities;


import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity

@DiscriminatorValue("CE")

public class CompteEparne extends Compte {
	
	private static final long serialVersionUID = -6452828516094496995L;
	private double taux ;

	public double getTaux() {
		return taux;
	}

	public void setTaux(double taux) {
		this.taux = taux;
	}

	public CompteEparne(double taux) {
		super();
		this.taux = taux;
	}

	public CompteEparne() {
		super();
	} 
	
	

}
