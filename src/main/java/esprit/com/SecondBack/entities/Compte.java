package esprit.com.SecondBack.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Proxy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;




@Entity

//Jpa
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="TYPE_CPTE",
discriminatorType=DiscriminatorType.STRING,length=4)


//mapping objet json
//Id.name Basé sur le nom de proptiéte pour faire la distinction entre le compte courant et le compte eparne 
@JsonTypeInfo(use = com.fasterxml.jackson.annotation.JsonTypeInfo.Id.NAME,include =  As.PROPERTY,property = "type")
@JsonSubTypes({
	@Type(name="CC",value=CompteCourant.class),
	@Type(name="CE",value =CompteEparne.class)
})
@Proxy(lazy = false)
public abstract class Compte  implements Serializable{

	private static final long serialVersionUID = 1852515658400897350L;
	
	@Id 
	private String numCompte ;
	
	private Date DateCration ; 
	
	private long solde ;
	
	@ManyToOne
    
	private Client client ;
	
	@OneToMany(mappedBy="compte",cascade = CascadeType.REMOVE,fetch = FetchType.EAGER)
	private Collection<Operation>operations ;
	
	
	@ManyToOne
		
	private Employe employe ;


	public String getNumCompte() {
		return numCompte;
	}


	public void setNumCompte(String numCompte) {
		this.numCompte = numCompte;
	}


	public Date getDateCration() {
		return DateCration;
	}


	public void setDateCration(Date dateCration) {
		DateCration = dateCration;
	}


	public long getSolde() {
		return solde;
	}


	public void setSolde(long solde) {
		this.solde = solde;
	}


	public Client getClient() {
		return client;
	}


	public void setClient(Client client) {
		this.client = client;
	}

	@JsonIgnore
	public Collection<Operation> getOperations() {
		return operations;
	}


	public void setOperations(Collection<Operation> operations) {
		this.operations = operations;
	}


	public Employe getEmploye() {
		return employe;
	}


	public void setEmploye(Employe employe) {
		this.employe = employe;
	}


	public Compte() {
		super();
	}
	
	
	
	
	

	
}
