package esprit.com.SecondBack.entities;


import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity

@DiscriminatorValue("CC")

public class CompteCourant extends Compte {

	private static final long serialVersionUID = 2038562720598052816L;
	private double decouvert ;

	public CompteCourant() {
		super();
	}

	public double getDecouvert() {
		return decouvert;
	}

	public void setDecouvert(double decouvert) {
		this.decouvert = decouvert;
	}

	public CompteCourant(double decouvert) {
		super();
		this.decouvert = decouvert;
	}

	

	
	
	
	

}
