package esprit.com.SecondBack.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;

@Entity
public class Employe implements Serializable {

	private static final long serialVersionUID = 932993057519556807L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
	private long codeEmploye ; 
	
	private String nomEmplye ;
	
	@OneToOne(mappedBy = "employe")
	private Operation operation ;
	
	@OneToMany(mappedBy = "employe",cascade = CascadeType.REMOVE,fetch = FetchType.EAGER)
	private Set<Compte>comptes ;
	
	
	@ManyToMany(mappedBy = "employes",cascade = CascadeType.REMOVE,fetch = FetchType.EAGER)
	
	private Set<Groupe>groupes ;
	
	 @ManyToOne
	  @JoinColumn(name="CODE_EMP_SUP")
	  private Employe employeSup;


	 
	@JsonIgnore
	public Employe getEmployeSup() {
		return employeSup;
	}

	@JsonSetter
	public void setEmployeSup(Employe employeSup) {
		this.employeSup = employeSup;
	}


	public long getCodeEmploye() {
		return codeEmploye;
	}


	public void setCodeEmploye(long codeEmploye) {
		this.codeEmploye = codeEmploye;
	}


	public String getNomEmplye() {
		return nomEmplye;
	}


	public void setNomEmplye(String nomEmplye) {
		this.nomEmplye = nomEmplye;
	}


	public Operation getOperation() {
		return operation;
	}


	public void setOperation(Operation operation) {
		this.operation = operation;
	}

	
	
	@JsonIgnore
	public Collection<Compte> getComptes() {
		return comptes;
	}


	public void setComptes(Set<Compte> comptes) {
		this.comptes = comptes;
	}

	
	@JsonIgnore
	public Collection<Groupe> getGroupes() {
		return groupes;
	}


	public void setGroupes(Set<Groupe> groupes) {
		this.groupes = groupes;
	}


	public Employe() {
		super();
	}
	
	
	
	

	

	
	
	
	
	

}
