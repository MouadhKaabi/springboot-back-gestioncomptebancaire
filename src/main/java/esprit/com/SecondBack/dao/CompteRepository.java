package esprit.com.SecondBack.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import esprit.com.SecondBack.entities.Compte;

public interface CompteRepository extends JpaRepository<Compte, String> {

}
