package esprit.com.SecondBack.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import esprit.com.SecondBack.entities.Employe;

public interface EmployeRepository extends JpaRepository<Employe, Long> {

}
