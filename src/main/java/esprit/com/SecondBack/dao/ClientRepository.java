package esprit.com.SecondBack.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import esprit.com.SecondBack.entities.Client;

public interface ClientRepository extends JpaRepository<Client, Long> {

}
