package esprit.com.SecondBack.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import esprit.com.SecondBack.entities.Operation;

public interface OperationRepository extends JpaRepository<Operation, Long> {

}
