package esprit.com.SecondBack.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import esprit.com.SecondBack.entities.Groupe;

public interface GroupeRepository  extends JpaRepository<Groupe, Long> {

}
