package esprit.com.SecondBack.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import esprit.com.SecondBack.entities.Employe;
import esprit.com.SecondBack.metier.EmployeMetier;

@RestController
public class EmployeRestService {
	
	@Autowired
	
	private EmployeMetier employeMetier ;
	
	
	@RequestMapping(value = "/employe",method = RequestMethod.POST)
	public Employe AjouterEmpl(@RequestBody Employe e) {
		return employeMetier.AjouterEmpl(e);
	}
	@RequestMapping(value = "/employe",method = RequestMethod.GET)
	public List<Employe> listemplEmployes() {
		return employeMetier.listemplEmployes();
	}
	
	

}
