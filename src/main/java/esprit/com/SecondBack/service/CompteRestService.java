package esprit.com.SecondBack.service;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import esprit.com.SecondBack.entities.Compte;
import esprit.com.SecondBack.metier.CompteMetier;

@RestController
public class CompteRestService {
	
	@Autowired 
	private CompteMetier compteMetier ;

	
	@RequestMapping(value = "/compte",method = RequestMethod.POST)
	public Compte AjouterCompte(@RequestBody Compte c) {
		return compteMetier.AjouterCompte(c);
	}

	@RequestMapping(value = "/compte/{numCompte}",method = RequestMethod.GET)
	public Compte getCompte(@PathVariable String numCompte) {
		return compteMetier.getCompte(numCompte);
	}
	
	
	

}
