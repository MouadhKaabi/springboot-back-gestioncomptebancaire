package esprit.com.SecondBack.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.RestController;

import esprit.com.SecondBack.entities.Client;
import esprit.com.SecondBack.metier.ClientMetier;

@RestController
public class ClientRestsService {
	// injection de dependance 
	@Autowired
	private ClientMetier clientMetier ;
	//creation des methodes par delegation
	//request body indique a spring il devoit recuperer les donnes de client dans le corps de la requette au format json 

	@RequestMapping(value = "/client",method = RequestMethod.POST)
	
	public Client AjouterClient(@RequestBody Client c) {
		return clientMetier.AjouterClient(c);
	}

	@RequestMapping(value = "/client",method = RequestMethod.GET)
	public List<Client> listClient() {
		return clientMetier.listClient();
	}

}
