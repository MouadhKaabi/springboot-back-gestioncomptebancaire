package esprit.com.SecondBack.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import esprit.com.SecondBack.metier.OperationMetier;

@RestController
public class OperationRestService {
	
	@Autowired
	private OperationMetier operationMetier ;
	
	
	@RequestMapping(value = "/versement",method = RequestMethod.PUT)
	public boolean verser(@RequestParam String numCompte, @RequestParam double montant,@RequestParam long codeEmploye) {
		return operationMetier.verser(numCompte, montant, codeEmploye);
	}

	@RequestMapping(value = "/retrait",method = RequestMethod.PUT)
	public boolean retirer(@RequestParam String numCompte,@RequestParam double montant,@RequestParam long codeEmploye) {
		return operationMetier.retirer(numCompte, montant, codeEmploye);
	}
	
	@RequestMapping(value = "/virement",method = RequestMethod.PUT)
	public boolean virement(@RequestParam String cpt1,@RequestParam String cpt2,@RequestParam double montant,@RequestParam long codeEmploye) {
		return operationMetier.virement(cpt1, cpt2, montant, codeEmploye);
	}

}
